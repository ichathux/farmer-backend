package com.controlunion.excelUploader.service;

import com.controlunion.excelUploader.model.FarmerList;
import com.controlunion.excelUploader.model.FarmerListCrop;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class JDBCBatchServiceFarmerListFinal {

    private final EntityManager entityManager;
    private final FarmerListService farmerListService;

    @Transactional
    public void farmerListFinalBatchInsert(int proId, int auditId) {

        ArrayList<FarmerList> farmerLists = farmerListService.getFarmListForProIdAndAuditId2(
                proId,
                auditId);

        System.out.println("Start saving data to db ");

        entityManager.unwrap(Session.class).doWork(connection -> {

            String delete_sql = "DELETE FROM `farmerlist_final` WHERE `proID` = " + proId + " and auditID='" + auditId + "'";

            String farmer_final_sql = "INSERT INTO `farmerlist_final` (`cufarmerID`,`auditID`, `proID`, `unitNoEUJAS`, `farCodeEUJAS`," +
                    " `unitNoNOP`, `farCodeNOP`, `farmerName`, `farmName`, `plotCode`, `totalArea`, `gps`, "
                    + " `address`, `city`, `dateCert`, `aplyRetrospe`, `certification`, `fertilizer`, `ferUseDate`," +
                    " `note`, `user`, `dateConfersion`, `dateorganic`, `eujas_field`, `eujas_harvest`," +
                    " `usda_field`, `usda_harvest`,`listid`)"
                    + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

            String crop_sql = "INSERT INTO `farmerlist_crop_final` ( `listid`, `auditID`, `proID`, `cropID`, `noOfPlant`, `estiYield`, `realYield`, `cufarmerID`, `noOfSesons`, `plotCode`)\n"
                    + "VALUES (?,?,?,?,?,?,?,?,?,?);";

            String plan_sql = "UPDATE `plan` SET `certified` = 1, `uploadBy` = ?, `user` = ? WHERE `planID` = ?;";

            try (PreparedStatement farmerData = connection.prepareStatement(farmer_final_sql);
                 PreparedStatement farmerListCrop = connection.prepareStatement(crop_sql);
                 PreparedStatement farmerList_final_delete = connection.prepareStatement(delete_sql);
                 PreparedStatement updatePlan = connection.prepareStatement(plan_sql)) {

//                deleting existing data
                System.out.println("deleting existing data");

                farmerList_final_delete.execute();

                for (FarmerList entity : farmerLists) {

                    farmerData.setInt(1, entity.getCufarmerID());
                    farmerData.setInt(2, entity.getAuditID());
                    farmerData.setInt(3, entity.getProID());
                    farmerData.setString(4, entity.getUnitNoEUJAS());
                    farmerData.setString(5, entity.getFarCodeEUJAS());
                    farmerData.setString(6, entity.getUnitNoNOP());
                    farmerData.setString(7, entity.getFarCodeNOP());
                    farmerData.setString(8, entity.getFarmerName());
                    farmerData.setString(9, entity.getFarmName());
                    farmerData.setString(10, entity.getPlotCode());
                    farmerData.setDouble(11, entity.getTotalArea());
                    farmerData.setString(12, entity.getGps());
                    farmerData.setString(13, entity.getAddress());
                    farmerData.setString(14, entity.getCity());
                    if (entity.getDateCert() == null){
                        farmerData.setDate(15, null);

                    }else{
                        farmerData.setDate(15, new java.sql.Date(entity.getDateCert().getTime()));

                    }
                    farmerData.setInt(16, entity.getAplyRetrospe());
                    farmerData.setString(17, entity.getCertification());
                    farmerData.setString(18, entity.getFertilizer());
                    farmerData.setString(19, entity.getFerUseDate());
                    farmerData.setString(20, entity.getNote());
                    farmerData.setString(21, "isuru");

                    farmerData.setDate(22, entity.getDateConfersion());
                    farmerData.setDate(23, entity.getDateorganic());
                    farmerData.setString(24, entity.getEujas_field().toUpperCase());
                    farmerData.setString(25, entity.getEujas_harvest().toUpperCase());
                    farmerData.setString(26, entity.getUsda_field().toUpperCase());
                    farmerData.setString(27, entity.getUsda_harvest().toUpperCase());
                    farmerData.setInt(28, entity.getListid());

                    farmerData.addBatch();
                    farmerData.clearParameters();

                    List<FarmerListCrop> farmerListCrops = entity.getFarmerListCropList();
//                    System.out.println("Crop list size : "+farmerListCrops.size());
                    for (FarmerListCrop flCrops : farmerListCrops) {

                        farmerListCrop.setInt(1, entity.getListid());
                        farmerListCrop.setInt(2, entity.getAuditID());
                        farmerListCrop.setInt(3, entity.getProID());
                        farmerListCrop.setInt(4, flCrops.getCropID());
                        farmerListCrop.setDouble(5, flCrops.getNoOfPlant());
                        farmerListCrop.setDouble(6, flCrops.getEstiYield());
                        farmerListCrop.setDouble(7, flCrops.getRealYield());
                        farmerListCrop.setInt(8, flCrops.getCufarmerID());
                        farmerListCrop.setDouble(9, flCrops.getNoOfSesons());
                        farmerListCrop.setString(10, flCrops.getPlotCode());
                        farmerListCrop.addBatch();
                        farmerListCrop.clearParameters();
                    }
                    farmerData.clearParameters();
                }
                farmerData.executeLargeBatch();
                farmerData.clearBatch();

                farmerListCrop.executeLargeBatch();
                farmerListCrop.clearBatch();

                updatePlan.setString(1, "isuru");
                updatePlan.setString(2, "isuru");
                updatePlan.setInt(3, auditId);
                updatePlan.execute();

            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }
}
