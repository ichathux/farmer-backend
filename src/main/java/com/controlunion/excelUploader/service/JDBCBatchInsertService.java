package com.controlunion.excelUploader.service;

import com.controlunion.excelUploader.model.FarmerList;
import com.controlunion.excelUploader.model.FarmerListCrop;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
@Slf4j
public class JDBCBatchInsertService {

    private final EntityManager entityManager;

    @Transactional
    public void farmerListBatchInsert(int proId, int auditId, ArrayList<FarmerList> entities, Long lastAuditId) {

        System.out.println("init list got to save " + entities.size());
        System.out.println("Start saving data to db ");
        deleteIfExist(proId, auditId);

        entityManager.unwrap(Session.class).doWork(connection -> {
            String farmer_sql = "INSERT INTO `farmerlist` (`cufarmerID`,`auditID`, `proID`, `unitNoEUJAS`, `farCodeEUJAS`," +
                    " `unitNoNOP`, `farCodeNOP`, `farmerName`, `farmName`, `plotCode`, `totalArea`, `gps`, "
                    + " `address`, `city`, `dateCert`, `aplyRetrospe`, `certification`, `fertilizer`, `ferUseDate`," +
                    " `note`, `user`, `isNew`, `dateConfersion`, `dateorganic`, `eujas_field`, `eujas_harvest`," +
                    " `usda_field`, `usda_harvest`,listid,isChange,chng_farmdata,chng_cropdata)\n"
                    + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

            String crop_sql = "INSERT INTO `farmerlist_crop` ( `listid`, `auditID`, `proID`, `cropID`, `noOfPlant`, `estiYield`, `realYield`, `cufarmerID`, `noOfSesons`, `plotCode`)\n"
                    + "VALUES (?,?,?,?,?,?,?,?,?,?);";

            String deletedF_sql = "delete FROM `farmerlist_deleted` WHERE `proID` = " + proId;

            ArrayList<FarmerList> deletedFarmerLists = new ArrayList<>();
            try (PreparedStatement deleteDeletedList = connection.prepareStatement(deletedF_sql);
                 PreparedStatement farmerData = connection.prepareStatement(farmer_sql);
                 PreparedStatement farmerListCrop = connection.prepareStatement(crop_sql)) {

                System.out.println("deleting old crop delete list " + proId + " : " + auditId);
                deleteDeletedList.execute();
                System.out.println("deleted old crop delete list");
                System.out.println("deleting deleted list" + proId + " : " + auditId);
                System.out.println("deleting deleted list done");
                int i = 1;

                for (FarmerList entity : entities) {
                    if (entity.getIsChange() == 3) {
                        deletedFarmerLists.add(entity);
                        continue;
                    }
                    farmerData.setInt(1, entity.getCufarmerID());
                    farmerData.setInt(2, entity.getAuditID());
                    farmerData.setInt(3, entity.getProID());
                    farmerData.setString(4, entity.getUnitNoEUJAS());
                    farmerData.setString(5, entity.getFarCodeEUJAS());
                    farmerData.setString(6, entity.getUnitNoNOP());
                    farmerData.setString(7, entity.getFarCodeNOP());
                    farmerData.setString(8, entity.getFarmerName());
                    farmerData.setString(9, entity.getFarmName());
                    farmerData.setString(10, entity.getPlotCode());
                    farmerData.setDouble(11, entity.getTotalArea());
                    farmerData.setString(12, entity.getGps());
                    farmerData.setString(13, entity.getAddress());
                    farmerData.setString(14, entity.getCity());
                    if (entity.getDateCert() == null) {
                        farmerData.setDate(15, null);
                    } else {
                        farmerData.setDate(15, new java.sql.Date(entity.getDateCert().getTime()));
                    }
                    farmerData.setInt(16, entity.getAplyRetrospe());
                    farmerData.setString(17, entity.getCertification());
                    farmerData.setString(18, entity.getFertilizer());
                    farmerData.setString(19, entity.getFerUseDate());
                    farmerData.setString(20, entity.getNote());
                    farmerData.setString(21, "isuru");
                    farmerData.setInt(22, entity.getIsNew());
                    farmerData.setDate(23, entity.getDateConfersion());
                    farmerData.setDate(24, entity.getDateorganic());

                    farmerData.setString(25, entity.getEujas_field() == null ?
                            "null" : entity.getEujas_field().toUpperCase());

                    farmerData.setString(26, entity.getEujas_harvest() == null ?
                            "null" : entity.getEujas_harvest().toUpperCase());

                    farmerData.setString(27, entity.getUsda_field() == null ?
                            "null" : entity.getUsda_field().toUpperCase());

                    farmerData.setString(28, entity.getUsda_harvest() == null ?
                            "null" : entity.getUsda_harvest().toUpperCase());

                    farmerData.setInt(29, entity.getListid());
                    farmerData.setInt(30, entity.getIsChange());
                    if (entity.getIsNew() == 1) {
                        farmerData.setString(31, "New Plot");
                        farmerData.setString(32, "");
                    } else {
                        farmerData.setString(31, (entity.getChngFarmdata()));
                        farmerData.setString(32, (entity.getChngCropdata()));
                    }
//                    farmerData.setString(33,entity.getChngVersion());

                    farmerData.addBatch();
                    farmerData.clearParameters();
//                    System.out.println(i+" : "+entity.getFarCodeEUJAS());
//                    System.out.println(entity);

                    List<FarmerListCrop> farmerListCrops = entity.getFarmerListCropList();
                    for (FarmerListCrop flCrops : farmerListCrops) {
                        farmerListCrop.setInt(1, entity.getListid());
                        farmerListCrop.setInt(2, entity.getAuditID());
                        farmerListCrop.setInt(3, entity.getProID());
                        farmerListCrop.setInt(4, flCrops.getCropID());
                        farmerListCrop.setDouble(5, flCrops.getNoOfPlant());
                        farmerListCrop.setDouble(6, flCrops.getEstiYield());
                        farmerListCrop.setDouble(7, flCrops.getRealYield());
                        farmerListCrop.setInt(8, flCrops.getCufarmerID());
                        farmerListCrop.setDouble(9, flCrops.getNoOfSesons());
                        farmerListCrop.setString(10, flCrops.getPlotCode());
                        farmerListCrop.addBatch();
                        farmerListCrop.clearParameters();
                    }
                    farmerData.clearParameters();
//                    }
                    i++;
                }
                System.out.println(i + " lines writes");
                farmerData.executeLargeBatch();
                farmerData.clearBatch();
                System.out.println("farm data save done");
                farmerListCrop.executeLargeBatch();
                farmerListCrop.clearBatch();
                System.out.println("farm crop data save done");
                if (!deletedFarmerLists.isEmpty()) {
                    deleted(auditId, deletedFarmerLists);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void deleteIfExist(int proId, int auditId) {
        String delete_sql = "delete FROM `farmerlist` WHERE `proID` = " + proId + " and auditID='" + auditId + "'";
        entityManager.unwrap(Session.class).doWork(connection -> {
            try (PreparedStatement dltData = connection.prepareStatement(delete_sql)) {
                dltData.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }


    private void deleted(int auditId, ArrayList<FarmerList> farmerLists) {

        entityManager.unwrap(Session.class).doWork(connection -> {
            String farmer_sql = "INSERT INTO `farmerlist_deleted` (`cufarmerID`,`auditID`, `proID`, `unitNoEUJAS`, `farCodeEUJAS`," +
                    " `unitNoNOP`, `farCodeNOP`, `farmerName`, `farmName`, `plotCode`, `totalArea`, `gps`, "
                    + " `address`, `city`, `dateCert`, `aplyRetrospe`, `certification`, `fertilizer`, `ferUseDate`," +
                    " `note`, `user`, `dateConfersion`, `dateorganic`, `eujas_field`, `eujas_harvest`," +
                    " `usda_field`, `usda_harvest`,listid)\n"
                    + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

            String crop_sql = "INSERT INTO `farmerlist_crop_deleted` ( `listid`, `auditID`, `proID`, `cropID`, `noOfPlant`, `estiYield`, `realYield`, `cufarmerID`, `noOfSesons`, `plotCode`, `id`)\n"
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?);";

            try (PreparedStatement farmerData = connection.prepareStatement(farmer_sql);
                 PreparedStatement farmerListCrop = connection.prepareStatement(crop_sql)) {
                for (FarmerList farmerList : farmerLists) {
                    farmerData.setInt(1, farmerList.getCufarmerID());
                    farmerData.setInt(2, auditId);
                    farmerData.setInt(3, farmerList.getProID());
                    farmerData.setString(4, farmerList.getUnitNoEUJAS());
                    farmerData.setString(5, farmerList.getFarCodeEUJAS());
                    farmerData.setString(6, farmerList.getUnitNoNOP());
                    farmerData.setString(7, farmerList.getFarCodeNOP());
                    farmerData.setString(8, farmerList.getFarmerName());
                    farmerData.setString(9, farmerList.getFarmName());
                    farmerData.setString(10, farmerList.getPlotCode());
                    farmerData.setDouble(11, farmerList.getTotalArea());
                    farmerData.setString(12, farmerList.getGps());
                    farmerData.setString(13, farmerList.getAddress());
                    farmerData.setString(14, farmerList.getCity());
                    if (farmerList.getDateCert() == null) {
                        farmerData.setDate(15, null);
                    } else {
                        farmerData.setDate(15, farmerList.getDateCert());
                    }
//                    farmerData.setDate(15, new java.sql.Date(farmerList.getDateCert().getTime()));
                    farmerData.setInt(16, farmerList.getAplyRetrospe());
                    farmerData.setString(17, farmerList.getCertification());
                    farmerData.setString(18, farmerList.getFertilizer());
                    farmerData.setString(19, farmerList.getFerUseDate());
                    farmerData.setString(20, farmerList.getNote());
                    farmerData.setInt(21, farmerList.getIsNew());
                    farmerData.setDate(22, farmerList.getDateConfersion());
                    farmerData.setDate(23, farmerList.getDateorganic());
                    farmerData.setString(24, farmerList.getEujas_field().toUpperCase());
                    farmerData.setString(25, farmerList.getEujas_harvest().toUpperCase());
                    farmerData.setString(26, farmerList.getUsda_field().toUpperCase());
                    farmerData.setString(27, farmerList.getUsda_harvest().toUpperCase());
                    farmerData.setInt(28, farmerList.getListid());

                    List<FarmerListCrop> farmerListCrops = farmerList.getFarmerListCropList();

                    for (FarmerListCrop flCrops : farmerListCrops) {
                        farmerListCrop.setInt(1, farmerList.getListid());
                        farmerListCrop.setInt(2, auditId);
                        farmerListCrop.setInt(3, farmerList.getProID());
                        farmerListCrop.setInt(4, flCrops.getCropID());
                        farmerListCrop.setDouble(5, flCrops.getNoOfPlant());
                        farmerListCrop.setDouble(6, flCrops.getEstiYield());
                        farmerListCrop.setDouble(7, flCrops.getRealYield());
                        farmerListCrop.setInt(8, flCrops.getCufarmerID());
                        farmerListCrop.setDouble(9, flCrops.getNoOfSesons());
                        farmerListCrop.setString(10, flCrops.getPlotCode());
                        farmerListCrop.setInt(11, flCrops.getId());
                        farmerListCrop.addBatch();
                        farmerListCrop.clearParameters();
                    }

                    farmerData.addBatch();
                    farmerData.clearParameters();
                    farmerData.clearParameters();
                }
                farmerData.executeLargeBatch();
                farmerData.clearBatch();

                farmerListCrop.executeLargeBatch();
                farmerData.clearParameters();
            }

        });
    }


}