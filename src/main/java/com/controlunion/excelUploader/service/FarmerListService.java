package com.controlunion.excelUploader.service;

import com.controlunion.excelUploader.dto.ComparisonResponseDto;
import com.controlunion.excelUploader.mapper.FarmerlistCropMapper;
import com.controlunion.excelUploader.mapper.FarmerlistCropMapperImpl;
import com.controlunion.excelUploader.mapper.FarmerlistMapper;
import com.controlunion.excelUploader.mapper.FarmerlistMapperImpl;
import com.controlunion.excelUploader.model.*;
import com.controlunion.excelUploader.repository.FarmerlistRepository;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class FarmerListService {

    private final FarmerlistRepository farmerListRepository;
    private final CropService cropService;
    private final JDBCBatchInsertService jdbcBatchInsertService;
    private final FarmerListDeletedService farmerListDeletedService;
    private final EntityManager entityManager;

    private final HikariDataSource hikariDataSource;

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void saveFarmerList(int proId, int auditId, Iterable<FarmerList> farmerLists, long lastAuditId) {
//        farmerListRepository.flush();
        ArrayList<FarmerList> farmerLists1 = new ArrayList<>();
        farmerLists.forEach(s -> farmerLists1.add(s));


        jdbcBatchInsertService.farmerListBatchInsert(proId, auditId, farmerLists1, lastAuditId);
//        ArrayList<FarmerListCrop_deleted> farmerListCrop_deleteds = new ArrayList<>();
////
//        ArrayList<FarmerList_deleted> list = farmerLists1.stream()
//                .filter(r -> r.getIsChange() == 3)
//                .map(f -> {
//                    ArrayList<FarmerListCrop_deleted> flcd = f.getFarmerListCropList().stream()
//                            .map(FarmerlistCropMapper.INSTANCE::farmerListCropToFarmerListCropDeleted)
//                            .collect(Collectors.toCollection(ArrayList::new));
//                    farmerListCrop_deleteds.addAll(flcd);
//                    return FarmerlistMapper.INSTANCE.farmerListToFarmerListDelete(f);
//                })
//                .collect(Collectors.toCollection(ArrayList::new));
////
//        ArrayList<FarmerList> list3 = farmerLists1.stream()
//                .filter(r -> r.getIsChange() != 3)
//                .collect(Collectors.toCollection(ArrayList::new));
//
////        ArrayList<FarmerList_deleted> listCrop = farmerLists1.stream()
////                .filter(r -> r.getIsChange() == 3)
////                .map(f -> f.getFarmerListCropList().stream().)
////                .collect(Collectors.toCollection(ArrayList::new));
//
//        System.out.println("deleting old data");
//        farmerListRepository.deleteAllByProIDAndAuditID(proId, auditId);
//        entityManager.setFlushMode(FlushModeType.COMMIT);
//        entityManager.flush();
//        entityManager.clear();
//        System.out.println("deleting old data from deleted");
//        farmerListDeletedService.deleteAllByProIdAndAuditId(proId, auditId);
//        entityManager.setFlushMode(FlushModeType.COMMIT);
//        entityManager.flush();
//        entityManager.clear();
//        log.info("save new farmer lists");
//        List<FarmerList> list1;

//        int batchSize = 30;
//
//        saveParallel(list3, batchSize);
//        if (list3.size() > 500) {
//            for (int i = 0; i < list3.size() + batchSize; i += batchSize) {
//                int endIndex = Math.min(i + batchSize, list3.size());
//                if ((list3.size() - i) < 0){
//                    break;
//                }
//                list1 = list3.subList(i, endIndex);
//                farmerListRepository.saveAllAndFlush(list1);
//                entityManager.setFlushMode(FlushModeType.COMMIT);
//                entityManager.flush();
//                entityManager.clear();
//            }
//        } else {
//            farmerListRepository.saveAllAndFlush(list3);
//            entityManager.setFlushMode(FlushModeType.COMMIT);
//            entityManager.flush();
//            entityManager.clear();
//        }

//        farmerListDeletedService.saveDeletedFarmers(list, farmerListCrop_deleteds);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void saveParallel(ArrayList<FarmerList> farmerLists, int batchSize) {
//        if (farmerLists.size() > 1000) {
        ArrayList<List<FarmerList>> list1 = new ArrayList<>();

        for (int i = 0; i < farmerLists.size() + batchSize; i += batchSize) {
            int endIndex = Math.min(i + batchSize, farmerLists.size());
            if ((farmerLists.size() - i) < 0) {
                break;
            }
            list1.add(farmerLists.subList(i, endIndex));


//            farmerListRepository.saveAllAndFlush(list1);
//            entityManager.setFlushMode(FlushModeType.COMMIT);
//            entityManager.flush();
//            entityManager.clear();
        }

//            int numThreads = 3;

//            ExecutorService executorService = Executors.newFixedThreadPool(numThreads);
//            System.out.println("saving data by "+batchSize);
//            for (List<FarmerList> entity : list1) {
////                System.out.println("running on loop");
//                executorService.submit(() -> {
//                    farmerListRepository.saveAllAndFlush(entity);
//                    entityManager.setFlushMode(FlushModeType.COMMIT);
//                    entityManager.flush();
//                    entityManager.clear();
//                });
//            }
//            executorService.shutdown();
//            try {
//                // Wait for all threads to finish
//                executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
//            } catch (InterruptedException e) {
//                Thread.currentThread().interrupt();
//            }
        System.out.println("max poll size : " + hikariDataSource.getMaximumPoolSize());
        System.out.println("nam : " + hikariDataSource.getJdbcUrl());

        System.out.println("starting executor");

        ExecutorService executorService = Executors
                .newFixedThreadPool((farmerLists.size()/batchSize)+(farmerLists.size()%batchSize));

        List<Callable<Void>> callables = list1.stream().map(sublist ->
                (Callable<Void>) () -> {
                    farmerListRepository.saveAllAndFlush(sublist);
                    entityManager.setFlushMode(FlushModeType.COMMIT);
                    entityManager.flush();
                    entityManager.clear();
                    return null;
                }).collect(Collectors.toList());
        try {

            executorService.invokeAll(callables);

            while (!executorService.isTerminated()){
                System.out.println("shutdowning : "+executorService.isShutdown());
                executorService.shutdown();
            }

            entityManager.close();
            entityManager.clear();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            System.out.println(executorService.isShutdown());;
            System.out.println("finally");
        }


//        } else {
//            farmerListRepository.saveAll(farmerLists);
//        }

    }

    public ComparisonResponseDto getFarmListForProIdAndAuditId(int proId, int auditId) {
        ComparisonResponseDto comparisonResponseDto = new ComparisonResponseDto();
        try {
            Optional<ArrayList<FarmerList>> farmerList = farmerListRepository.findAllByProIDAndAuditID(proId, auditId);
            if (farmerList.isPresent()) {
                List<Crop> crops = cropService.getAllCrops();

                ArrayList<FarmerList> farmerListsNew = new ArrayList<>();
                ArrayList<FarmerList> farmerListsOldChanged = new ArrayList<>();

                farmerList.get().stream()
                        .forEach(f ->
                                {
                                    if (f.getIsNew() == 1) {
                                        f.setChngCropdata(f.getFarmerListCropList().stream()
                                                .map(c -> crops.stream()
                                                        .filter(cl -> cl.getCropID() == c.getCropID())
                                                        .findFirst()
                                                        .orElse(new Crop())
                                                        .getCropName())
                                                .collect(Collectors.joining(", ")));
                                        farmerListsNew.add(f);
                                        System.out.println(f.getChngCropdata());
                                    } else if (f.getIsChange() == 1) {
                                        farmerListsOldChanged.add(f);
                                    }

                                }
                        );
                ArrayList<FarmerList_deleted> farmerListsDeleted = farmerListDeletedService.getAllByProIdAndAuditId(proId, auditId);

                System.out.println(farmerListsNew.size());
                System.out.println(farmerListsOldChanged.size());
                System.out.println(farmerListsDeleted.size());

//                ArrayList<FarmerList> farmerListsOldChanged = farmerList.get().stream()
//                        .filter(f -> f.getIsChange() == 1)
//                        .collect(Collectors.toCollection(ArrayList::new));
//
//                ArrayList<FarmerList> farmerListsDeleted = farmerList.get().stream()
//                        .filter(f -> f.getIsChange() == 3)
//                        .collect(Collectors.toCollection(ArrayList::new));

                comparisonResponseDto.setNewFarmerList(farmerListsNew);
                comparisonResponseDto.setExistingFarmerList(farmerListsOldChanged);
                comparisonResponseDto.setDeletedFarmerList(farmerListsDeleted);
            }
            return comparisonResponseDto;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<FarmerList> getFarmListForProIdAndAuditId2(int proId, int auditId) {
        try {
            System.out.println("getting farmerlist for " + proId + " " + auditId);
            Optional<ArrayList<FarmerList>> farmerList = farmerListRepository.findAllByProIDAndAuditID(proId, auditId);
            return farmerList.orElseGet(ArrayList::new);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

}
