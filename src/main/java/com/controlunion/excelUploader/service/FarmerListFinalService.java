package com.controlunion.excelUploader.service;

import com.controlunion.excelUploader.mapper.FarmerlistCropMapper;
import com.controlunion.excelUploader.mapper.FarmerlistMapper;
import com.controlunion.excelUploader.model.FarmerList;
import com.controlunion.excelUploader.model.FarmerListCrop;
import com.controlunion.excelUploader.model.FarmerListCropFinal;
import com.controlunion.excelUploader.model.FarmerListFinal;
import com.controlunion.excelUploader.repository.FarmerListFinalRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class FarmerListFinalService {

    private final FarmerListFinalRepository repository;
    private final Random random = new Random();

    public int createCuid() {
        try {
            int min = 100_000_000;
            int max = 999_999_999;

            int randomNineDigits = random.nextInt(max - min + 1) + min;
            int cuid = randomNineDigits;
            if (checkCUIDExist(cuid)) {
                return createCuid();
            }
            return cuid;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private boolean checkCUIDExist(int cuid) {
        try {
            return repository.existsByCufarmerID(cuid);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public ArrayList<Integer> getCuidList(){
        return repository.findDistinctCufarmerID().orElse(new ArrayList<>());
    }

    public ArrayList<FarmerListFinal> getBeforeCertifiedFarmLIstFinal(int proId, int auditId) {
        try {
            ArrayList<FarmerListFinal> farmerListFinals = repository.findAllByProIDAndAuditID(proId, auditId).orElse(null);
            log.info("getting farmerlist final for " + proId + " " + auditId + " size" + farmerListFinals.size());
            return farmerListFinals;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void deleteByCufarmerIDAndPlotCode(int cuid, String plotCode) {
        System.out.println("deleteing for " + cuid + " plot " + plotCode);
        repository.deleteByCufarmerIDAndPlotCode(cuid, plotCode);
    }
}
