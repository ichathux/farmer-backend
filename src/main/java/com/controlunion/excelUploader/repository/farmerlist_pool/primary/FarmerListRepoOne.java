package com.controlunion.excelUploader.repository.farmerlist_pool.primary;

import com.controlunion.excelUploader.model.FarmerList;
import com.controlunion.excelUploader.model.comp_keys.FarmerListID;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceContext;

public interface FarmerListRepoOne extends JpaRepository<FarmerList, FarmerListID> {
}
