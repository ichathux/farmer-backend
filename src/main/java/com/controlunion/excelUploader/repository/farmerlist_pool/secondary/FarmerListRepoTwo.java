package com.controlunion.excelUploader.repository.farmerlist_pool.secondary;

import com.controlunion.excelUploader.model.FarmerList;
import com.controlunion.excelUploader.model.comp_keys.FarmerListID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FarmerListRepoTwo extends JpaRepository<FarmerList, FarmerListID> {
}
