package com.controlunion.excelUploader.model;

import com.controlunion.excelUploader.model.comp_keys.FarmerListID;
import com.controlunion.excelUploader.model.custom.FarmerCommon;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

/**
 * @author : Udara Deshan <udaradeshan.ud@gmail.com>
 * @since : 10/31/2022
 **/

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "farmerlist_deleted")
@IdClass(FarmerListID.class)
public class FarmerList_deleted implements FarmerCommon {

    @Id
    @Column(name = "listid", nullable = true)
    private int listid;
    @Id
    @Column(name = "proID", nullable = false)
    private int proID;

    @Column(name = "plotCode", nullable = false, length = 100)
    private String plotCode;

    @Column(name = "unitNoEUJAS", nullable = false, length = 100)
    private String unitNoEUJAS;

    @Id
    @Column(name = "auditID", nullable = false)
    private int auditID;

    @Column(name = "newcuid")
    private Integer newCuid;

    @Column(name = "cufarmerID", nullable = false)
    private int cuFarmerId;

    @Column(name = "farCodeEUJAS", nullable = false, length = 150)
    private String farCodeEUJAS;

    @Column(name = "unitNoNOP", nullable = false, length = 100)
    private String unitNoNOP;

    @Column(name = "farCodeNOP", nullable = false, length = 100)
    private String farCodeNOP;

    @Column(name = "farmerName", nullable = false, length = 500)
    private String farmerName;

    @Column(name = "farmName", length = 600)
    private String farmName;

    @Column(name = "totalArea", nullable = false, precision = 18, scale = 3)
    private double totalArea;

    @Column(name = "city", nullable = false, length = 100)
    private String city;

    @Column(name = "gps", length = 100)
    private String gps;

    @Column(name = "address", length = 1000)
    private String address;

    @Column(name = "dateCert")
    private Date dateCert;

    @Column(name = "aplyRetrospe", nullable = false, columnDefinition = "TINYINT UNSIGNED")
    private int aplyRetrospe;

    @Column(name = "certification", nullable = false, length = 100)
    private String certification;

    @Column(name = "fertilizer", length = 1000)
    private String fertilizer;

    @Column(name = "ferUseDate", length = 100)
    private String ferUseDate;

    @Column(name = "dateConfersion")
    private Date dateConfersion;

    @Column(name = "dateorganic")
    private Date dateOrganic;

    @Column(name = "dateorganic_nop")
    private Date dateOrganicNop;

    @Column(name = "note", length = 500)
    private String note;

    @Column(name = "user", nullable = false, length = 100)
    private String user;

    @Column(name = "sysTimeStamp", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date sysTimeStamp;

    @Column(name = "eujas_field", nullable = false, length = 20)
    private String eujasField;

    @Column(name = "eujas_harvest", nullable = false, length = 20)
    private String eujasHarvest;

    @Column(name = "usda_field", nullable = false, length = 20)
    private String usdaField;

    @Column(name = "usda_harvest", nullable = false, length = 20)
    private String usdaHarvest;
}